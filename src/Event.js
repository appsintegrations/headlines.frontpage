import React from 'react'
import styled from 'styled-components'
import distanceInWords from 'date-fns/distance_in_words'
import parse from 'date-fns/parse'

const Container = styled('div')`
  width: 100%;
  display: flex;
  flex-direction: column;

  h1 {
    font-size: 6vh;
    margin-bottom: 6vh;
    line-height: 1.2em;
  }

  .summary {
    flex: 1 1 auto;
    overflow: hidden;
    margin-bottom: 8vh;

    h3 {
      font-size: 3.5vh;
      font-weight: 500;
      line-height: 1.3;
    }
  }

  p {
    font-size: 2vh;
    opacity: 0.7;
  }
`

const getDate = () => {
  try {
    return (parent.CCHDAPI || top.CCHDAPI).getLocalTime()
  } catch (e) {
    return new Date()
  }
}

const Event = ({ item, textColor, bgColor }) => {
  const currentDay = getDate()
  return (
    <Container background={bgColor} text={textColor}>
      <h1>{item.title}</h1>

      <div className="summary">
        <h3>{item.description}</h3>
      </div>

      {item.published_at && <p>{distanceInWords(currentDay, item.published_at)} ago</p>}
    </Container>
  )
}

export default Event
