import React from 'react'
import styled from 'styled-components'

import Headlines from './components/Headlines'

const Template = styled.div`
  width: 100vw;
  height: 100vh;
  position: relative;
  overflow: hidden;
`

const App = ({ events, settings, isAnimationEnabled }) => {
  if (!events) {
    console.log('No data returned to template')
    return <div />
  }
  return (
    <Template>
      <Headlines events={events} settings={settings} isAnimationEnabled={isAnimationEnabled} />
    </Template>
  )
}

export default App
