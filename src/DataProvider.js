import React from "react"

import reducer, { initialState } from "./reducer"

export const DataContext = React.createContext(initialState)
export const DataConsumer = DataContext.Consumer

export const transformImage = image => {
	return new Promise(resolve => {
		if (typeof image !== "object") return resolve(image)

		const reader = new FileReader()

		reader.onloadend = () => resolve(reader.result)

		reader.readAsDataURL(image)
	})
}

const DataProvider = ({ children }) => {
	const [state, dispatch] = React.useReducer(reducer, initialState)

	React.useEffect(() => {
		const handleLivePreview = async ({ data }) => {
			// Transform images from a File object to a base64 encoded string.
			if (data.theme && data.images) {
				if (data.images.logo) {
					data.theme.logo = await transformImage(data.images.logo)
				}

				if (data.images.background) {
					data.theme.background = await transformImage(data.images.background)
				}
			}

			dispatch(data)
		}

		window.addEventListener("message", handleLivePreview, "*")

		return () => window.removeEventListener("message", handleLivePreview)
	}, [])

	return <DataContext.Provider value={state}>{children}</DataContext.Provider>
}

export default DataProvider
