const getEnvData = () => {
  if (process.env.NODE_ENV !== 'production') {
    return require('../api/sample-data.json')
  }

  return window.__IWDATA__
}

export const initialState = getEnvData()

export default (state = initialState, action) => {
  switch (action.type) {
    case '__dashboardFieldChange':
    case '__dashboardSourceChange':
      const hasMultiSourceSupport = action.type === '__dashboardSourceChange'

      // action.payload.fields is here for customers running older version of headlines that do not support multiple-sources
      if (!action.payload.fields && !action.payload.sources) return state

      // action.payload.sources contains sources with their template_fields
      // if action.payload.sources doees not exist then we create an array of a fake source since there is only one source for older versions of headlines and that source's template_fields are available via action.payload.fields
      const sources = action.payload.sources || [{ template_fields: action.payload.fields || {} }]

      return {
        ...state,
        ...action.payload,
        data: sources.reduce((acc, { source_id, template_fields }) => {
          const keys = Object.keys(template_fields)

          // date_raw is an object with source_ids as keys and an array of that source's feed items as the values of each property. data_raw is an array of fee ditems when there is no payload.sources
          const data = hasMultiSourceSupport ? state.data_raw[source_id] : state.data_raw

          if (!data) return acc

          const itemsWithMappedKeys = data.map(item => {
            return keys.reduce((acc, key) => {
              acc[key] = item[template_fields[key]]

              return acc
            }, {})
          })

          return acc.concat(itemsWithMappedKeys)
        }, [])
      }

    case '__dashboardLivePreviewChange':
      return { ...state, ...action.payload }
    default:
      return state
  }
}
