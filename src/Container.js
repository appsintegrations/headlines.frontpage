import React from 'react'
import { ThemeProvider } from 'styled-components'

import { DataContext } from './DataProvider'
import App from './App'

const applyItemLimit = (data, limit) => {
  if (!Array.isArray(data)) return []
  if (!limit) return data

  return data.slice(0, limit)
}

const Container = () => {
  const { data, settings, slide, limit, isLegacyAsset } = React.useContext(DataContext)
  console.log(React.useContext(DataContext))

  const posts = applyItemLimit(data, limit)

  return (
    <ThemeProvider theme={settings || {}}>
      <App events={posts} settings={settings} slide={slide} isAnimationEnabled={!isLegacyAsset} />
    </ThemeProvider>
  )
}

export default Container
