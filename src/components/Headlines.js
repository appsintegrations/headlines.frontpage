import React, { useState, useEffect } from 'react'
import styled from 'styled-components'

import Background from './Headlines/Background'
import HeadlineContainer from './Headlines/HeadlineContainer'

const Template = styled.div`
  width: 100%;
  height: 100%;
`

const BackgroundContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`

const Logo = styled.img`
  position: absolute;
  top: 0;
  left: 3vw;
  height: 12vh;
  z-index: 2;
  box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.6);
`

const CopyrightContainer = styled.div`
  position: absolute;
  top: 3vh;
  right: 3vw;
  z-index: 2;

  span {
    color: ${({ theme }) => theme?.colors?.textColor};
    text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.6);
    font-family: 'Roboto-Light';
    font-size: 1.2em;
  }
`

const resetTimeout = (time) => {
  try {
    parent.CCHDAPI.resetSlideTimeout(time)
  } catch (e) {
    console.log('Unable to set slide timeout: not on a player')
  }
}

const getPostDelay = (settings) => {
  return settings?.slide?.postDelay >= 2 ? settings.slide.postDelay : 2
}

const Headlines = ({ events, settings, isAnimationEnabled }) => {
  const [slides, setSlides] = useState([])
  const [slideIndex, setSlideIndex] = useState(0)
  const [showLogo, setShowLogo] = useState(true)

  const nextSlideUrl = () => {
    const nextSlideIndex = slideIndex + 1 >= slides.length ? 0 : slideIndex + 1
    return slides[nextSlideIndex].image_url
  }

  useEffect(() => {
    setSlides(events.filter((e) => !!e.image_url))
  }, [events, setSlides])

  useEffect(() => {
    resetTimeout(slides.length * getPostDelay(settings))

    let interval = setInterval(() => {
      setSlideIndex((slideIndex) => {
        if (slideIndex + 1 >= slides.length) return 0
        return slideIndex + 1
      })
    }, getPostDelay(settings) * 1000)

    return () => {
      clearInterval(interval)
    }
  }, [slides, settings])

  if (!slides.length) return null

  const currentSlide = slides[slideIndex]
  return (
    <Template>
      {settings?.images?.logo && showLogo && <Logo src={settings.images.logo} onError={() => setShowLogo(false)} />}
      {currentSlide && currentSlide.image_copyright && (
        <CopyrightContainer>
          <span>{currentSlide.image_copyright}</span>
        </CopyrightContainer>
      )}
      {currentSlide && (
        <HeadlineContainer
          slide={currentSlide}
          postDelay={getPostDelay(settings)}
          isAnimationEnabled={isAnimationEnabled}
        />
      )}
      <BackgroundContainer>
        <Background
          url={currentSlide && currentSlide.image_url}
          nextUrl={nextSlideUrl()}
          settings={settings}
          lastParentRender={new Date()}
          isAnimationEnabled={isAnimationEnabled}
        />
      </BackgroundContainer>
    </Template>
  )
}

export default Headlines
