import React, { useState, useEffect } from 'react'
import styled, { keyframes, css } from 'styled-components'

const fadeDuration = 0.5

const burns = intensity => {
  return keyframes`
		from {
			transform: scale(1.0);
		}
		to {
			transform: scale(${intensity});
		}
	`
}

const fadeOut = keyframes`
    from {
        opacity: 1
    }
    to {
        opacity: 0
    }
`

const Template = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`

const BackgroundImageWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: ${props => (props.top ? 1 : 0)};
  animation: ${props =>
    props.fade
      ? css`
          ${fadeOut} ${fadeDuration}s linear
        `
      : 'none'}};
`

const BackgroundImage = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${({ theme }) => theme?.colors?.backgroundColor};
  background-image: ${props => (props.url ? `url(${props.url})` : 'none')};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  animation: ${props =>
    (props.top || props.endOfLife) && props.isAnimationEnabled
      ? css`
          ${burns(props.burnsIntensity)} ${props.burnsDuration}s linear
        `
      : 'none'};
`

const getPostDelay = settings => {
  return settings?.slide?.postDelay >= 2 ? settings.slide.postDelay : 2
}

const Background = ({
  url,
  nextUrl,
  settings,
  isAnimationEnabled
}) => {
  const [slides, setSlides] = useState([])
  const [animations, setAnimations] = useState(false)

  useEffect(() => {
    setSlides([
      {
        url,
        position: 'top'
      },
      {
        url: nextUrl,
        position: 'bottom'
      }
    ])

    setAnimations(animations => {
      return { ...animations, fadeOut: false }
    })

    // Start fading out the top card at the end of its lifecycle
    let endOfLifeTimer = setTimeout(() => {
      setAnimations(animations => {
        return { ...animations, fadeOut: true }
      })
    }, (getPostDelay(settings) - fadeDuration) * 1000)

    return () => {
      clearTimeout(endOfLifeTimer)
    }
  }, [url, nextUrl, settings])

  if (!slides) return <Template />
  return (
    <Template>
      {slides.map(({ url, position }) => {
        return (
          <BackgroundImageWrapper top={position === 'top'} fade={position === 'top' && isAnimationEnabled && animations.fadeOut} key={url}>
            <BackgroundImage
              url={url}
              top={position === 'top'}
              burnsDuration={getPostDelay(settings) + fadeDuration}
              burnsIntensity={settings?.slide?.scaleIntensity || 1.3}
              endOfLife={isAnimationEnabled && animations.fadeOut}
              isAnimationEnabled={isAnimationEnabled}
            />
          </BackgroundImageWrapper>
        )
      })}
    </Template>
  )
}

export default Background
