import React, { useState, useEffect, useRef } from 'react'
import styled, { css, keyframes } from 'styled-components'

const effectDuration = 1.0

const slideUp = dist => {
  return keyframes`
		from {
			transform: translateY(${dist}px)
		}

		to {
			transform: translateY(0px)
		}
	`
}

const slideDown = dist => {
  return keyframes`
		from {
			transform: translateY(0px)
		}

		to {
			transform: translateY(${dist}px)
		}
	`
}

const slideUpAnimation = motion => css`
  ${slideUp(motion.distance)} ${effectDuration}s ease-out
`

const slideDownAnimation = motion => css`
  ${slideDown(motion.distance)} ${effectDuration}s ease-out
`

const Template = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  min-height: 20vh;
  z-index: 2;
  background-color: rgba(0, 0, 0, 0.65);
  backdrop-filter: blur(8px);
  padding: 2.5vh 2.5vw 2.5vh 2.5vw;
  color: ${({ theme }) => theme?.colors?.textColor};
  ${({ isAnimationEnabled, isVisible, motion }) =>
    isAnimationEnabled &&
    css`
      bottom: ${isVisible ? '0' : '-50vh'};
      animation: ${motion.direction === 'up' ? slideUpAnimation(motion) : slideDownAnimation(motion)};
    `}
`

// Headline + description
const NewsContent = styled.div`
  h1 {
    margin-bottom: 2.5vh;
    font-family: 'Roboto-Bold';
    font-size: 2.6em;
    transform: scale(1, 1.35);
    line-height: 1em;
    max-height: 1em;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  span {
    display: inline-block;
    font-family: 'Roboto-Light';
    font-size: 1.6em;
    line-height: 1.2em;
    max-height: 3.6em;
    overflow: hidden;
  }
`

const SocialContent = styled.span`
  display: inline-block;
  font-family: 'Roboto-Light';
  font-size: 2em;
  line-height: 1.2em;
  max-height: 4.8em;
  overflow: hidden;
`

const formatDescription = (description, maxLength = 255) => {
  // Strip tags
  let stripped = description?.replace(/(<([^>]+)>)/gi, '')

  if (stripped) {
    // Strip line breaks
    stripped = stripped.replace(/(\r\n|\n|\r)/gm, ' ')
    stripped = stripped.replace('  ', ' ')
  }

  // Trim length
  const words = stripped?.split(' ') || []

  let output = ''
  for (let w of words) {
    if ((output + w).length <= maxLength) output += `${w} `
    else {
      output += '...'
      break
    }
  }

  return output
}

const HeadlineContainer = ({ slide, postDelay, isAnimationEnabled }) => {
  const [motion, setMotion] = useState({})

  const containerRef = useRef(null)
  useEffect(() => {
    const distance = containerRef.current.clientHeight
    setMotion({ direction: 'up', distance })

    let timeout = setTimeout(() => {
      setMotion({ direction: 'down', distance })
    }, (postDelay - effectDuration) * 1000)

    return () => {
      clearTimeout(timeout)
    }
  }, [slide, postDelay])

  return (
    <Template ref={containerRef} motion={motion} isVisible={motion.direction} isAnimationEnabled={isAnimationEnabled} hidden={!slide.title && !slide.description}>
      {slide.title && slide.description ? (
        <NewsContent>
          <h1>{slide.title.replace(/(<([^>]+)>)/gi, '').toUpperCase()}</h1>
          <span>{formatDescription(slide?.description, 460)}</span>
        </NewsContent>
      ) : (
        <React.Fragment>
          {!!slide.title && <SocialContent>{formatDescription(slide.title, 500)}</SocialContent>}
          {!!slide.description && <SocialContent>{formatDescription(slide.description, 500)}</SocialContent>}
        </React.Fragment>
      )}
    </Template>
  )
}

export default HeadlineContainer
